#!/usr/bin/env python3

import datetime
import os
import shutil
import subprocess
import zipfile

filename = ''
epson = "/dev/rtc"
oki = "/dev/printer"
rmCom = 'rm /home/cashier/.wine/dosdevices/com2'
lnLpt = 'ln -s /dev/prepson ~/.wine/dosdevices/lpt1'
fm1402 = 'wine /home/cashier/.wine/drive_c/FM1402/psFMUTools.exe'
Coupon = '/home/cashier/.wine/drive_c/PSTrade/Coupon0.txt'
mvCoupon = 'mv /home/cashier/.wine/drive_c/PSTrade/Coupon0.txt /home/cashier/.wine/drive_c/windows/system32/'
rmfm1402 = 'rm -r /home/cashier/.wine/drive_c/FM1402'
rmfiles = "rm -f /home/cashier/.wine/drive_c/PSTrade/*"
rmDB = 'rm -r /home/cashier/.wine/drive_c/PSTrade/DB'
rmReports = 'rm -r /home/cashier/.wine/drive_c/windows/system32/Reports*'
RM_AdmFrame='rm /home/cashier/.wine/drive_c/windows/system32/AdmFrame*'
RM_borlndmm='rm /home/cashier/.wine/drive_c/windows/system32/borlndmm*'
RM_Discount='rm /home/cashier/.wine/drive_c/windows/system32/Discount*'
RM_Weights='rm /home/cashier/.wine/drive_c/windows/system32/Weights*'
RM_PumpDLL='rm /home/cashier/.wine/drive_c/windows/system32/PumpDLL*'
RM_PsBackOffice='rm /home/cashier/.wine/drive_c/windows/system32/PsBackOffice*'
#Переменные
#папка с торговой программой PSTrade
PSTrade='/home/cashier/.wine/drive_c/PSTrade'
#папка  файлов фискалки
FM1402='/home/cashier/.wine/drive_c/FM1402'
#папка дисконтных карт
DISCOUNT_Folder='/home/cashier/.wine/drive_c/DiscountCard'
#Cashmain.ini файл настроек торговли
Cashmain_ini='/home/cashier/.wine/drive_c/windows/CashMain.ini'
#Cash.ini файл настрое пользователя и окон торговли
Cash_ini='/home/cashier/.wine/drive_c/windows/Cash.ini'
#папка с сертификатами VPN
VPN='/etc/openvpn'
#файл с настройками навесного оборудования
Devices='/etc/udev/rules.d/99-usb-serial.rules'
#Reports из System32
Reports='/home/cashier/.wine/drive_c/windows/system32/Reports'
#Берем текущуюю дату
GetDate=datetime.datetime.now().strftime("%d-%m-%Y-(%H-%M-%S)")
#имя Бэкапа
Name_BackUP='Auto-BackUP'
#папка для бэкап-файлов
Dir_BackUP='%s-%s' % (Name_BackUP, GetDate)
#папка для BackUP's
BackUP='/home/cashier/.wine/drive_c/%s/%s' % (Name_BackUP, Dir_BackUP)
#папка для BackUP's
BackUP_PSTrade='/home/cashier/.wine/drive_c/%s/%s/PSTrade/' % (Name_BackUP, Dir_BackUP)
#папка для BackUP's
BackUP_FM1402='/home/cashier/.wine/drive_c/%s/%s/FM1402/' % (Name_BackUP, Dir_BackUP)
#папка для BackUP's
BackUP_DISCOUNT_Folder='/home/cashier/.wine/drive_c/%s/%s/DiscountCard/' % (Name_BackUP, Dir_BackUP)
#папка для BackUP's
BackUP_VPN='/home/cashier/.wine/drive_c/%s/%s/openvpn/' % (Name_BackUP, Dir_BackUP)
#папка для BackUP's
BackUP_Reports='/home/cashier/.wine/drive_c/%s/%s/Reports/' % (Name_BackUP, Dir_BackUP)
#папка для BackUP's
BackUP_Sys32='/home/cashier/.wine/drive_c/%s/%s/Sys32/' % (Name_BackUP, Dir_BackUP)
#файл ShopName
Shop_Name='/opt/ShopName.txt'
#BackUP-system32
Sys32='/home/cashier/.wine/drive_c/%s/%s/Sys32' % (Name_BackUP, Dir_BackUP)
#путь к подключенной флэшке
FLASH_DIR='/media/cashier/'
#Диск C
DriveC='/home/cashier/.wine/drive_c/'
#
TMP_STR='/home/cashier/.wine/drive_c/Auto-BackUP/'
########## DLL
AdmFrame='/home/cashier/.wine/drive_c/PSTrade/AdmFrame.dll'
borlndmm='/home/cashier/.wine/drive_c/PSTrade/borlndmm.dll'
Discount='/home/cashier/.wine/drive_c/PSTrade/Discount.dll'
Weights='/home/cashier/.wine/drive_c/PSTrade/Weights.dll'
PumpDLL='/home/cashier/.wine/drive_c/PSTrade/PumpDLL.dll'
PsBackOffice='/home/cashier/.wine/drive_c/PSTrade/PsBackOffice.dll'
Error_TMP = '0'

def warning_process_copy(file, adress):
    global filename
    filename = subprocess.check_output(['zenity', '--warning', '--title=Ошибка!!!', '--text=Ошибка при копировании! %s\n'
                                                                    'в каталог %s' % ((file.split("/")[-1]), adress)])

def warning_error_copy_sknoFiles(file):
    global filename
    filename = subprocess.check_output(['zenity', '--warning', '--title=Ошибка!!!', '--text=Ошибка!\n %s в каталоге '
                                                 'Apteki_SKNO_install не найден!' % (file.split("/")[-1])])

def warning_error(x, y):
    global filename
    filename = subprocess.check_output(['zenity', '--warning', '--title=Ошибка!!!', '--text=Ошибка!\n %s не найден \n '
                                        'при копировании файлов в \n %s' % (x, y)])

def warning_error_FLASH():
    global filename
    filename = subprocess.check_output(['zenity', '--warning', '--title=Ошибка!!!', '--text=Ошибка %s пуст\n'
                                                                                 'нет смонтированных накопителей в систему\n'
                                                                                 'либо их больше одной \n'
                                                                                 'Смонтируйте накопитель в систему и запустите программу еще раз' % (FLASH_DIR)])

def copy_dir(x, y, z):
    if not os.path.exists(x):
        warning_error(x, y)
    else:
        shutil.copytree(x, z)

def copy_file(x, y, z):
    if not os.path.exists(x):
        warning_error(x, y)
    else:
        shutil.copy(x, z)

def remove_files():
    from subprocess import Popen, PIPE
    if not os.path.exists(Coupon):
        print("Купон в PSTrade не найден")
    else:
        proc = subprocess.Popen(mvCoupon, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
    proc = subprocess.Popen(rmfm1402, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
    proc = subprocess.Popen(rmfiles, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
    proc = subprocess.Popen(rmDB, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
    proc = subprocess.Popen(rmReports, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)

def remove_dll():
    from subprocess import Popen, PIPE
    proc = subprocess.Popen(RM_PsBackOffice, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
    proc = subprocess.Popen(RM_PumpDLL, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
    proc = subprocess.Popen(RM_Weights, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
    proc = subprocess.Popen(RM_AdmFrame, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
    proc = subprocess.Popen(RM_borlndmm, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
    proc = subprocess.Popen(RM_Discount, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)

def add_string_99usb():
        f = open(usb99_file, 'r')
        # Цикл построчного поиска нужной команды
        tmp_string = 0
        for oneline in f:
            oneline = oneline.strip(' \t\n\r ')
            # если не нашли нужный текст в строке ищем дальше
            if oneline.find(MCTA_vendor) == -1:
                continue
                # если нашли
            else:
                tmp_string = 1
                break
        f.close()
        if tmp_string == 0:
            f = open(usb99_file, 'a')
            f.write(MCTA_vendor + '\n')
        f.close()

def scview_pass():
    prev_string_oneline = ''
    f = open(Cash_ini, 'r', encoding="UTF-8")
    # Цикл построчного поиска нужной команды
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        if oneline.find('LoginNameEdit_Text') == -1:
            prev_string_oneline = oneline
            continue
        else:
            name = ('Имя пользователя = ' + oneline.split('=')[1])

    f = open(Cash_ini, 'r', encoding="UTF-8")
    for oneline in f:
        oneline = oneline.strip(' \t\n\r ')
        if oneline.find('PasswordBEdit_Text') == -1:
            prev_string_oneline = oneline
            continue
        else:
            passw = ('Пароль = ' + oneline.split('=')[1])

    f.close()

def step_1():
    global filename
    filename = subprocess.check_output(['zenity','--question', '--title=Установка СКНО ШАГ 1',
                                    '--text= "Внимание перед началом установки СКНО!!\n\n\n\n'
                                    '1.Проверьте равенство сумм в БФП и БД\n'
                                    '(в меню ПС-Торговля пункт Контроль данных БЭП)\n'
                                    '2.Проверьте сумму по Безналу по КСА с банковским терминалом\n'
                                    '3.Выгрузите все чеки за последние несколько смен\n'
                                    '4.Откройте окно выгрузки по Дисконтным картам и нажмите кнопку Выгрузить\n'
                                    '5.Обязательно сделайте инкасацию на всю сумму наличных\n(чтобы наличных по кассе было 0)\n'
                                    '6.Закройте смену\n\n'
                                    'После выполнения всех пунктов нажмите Далее.\nЕсли один из них не выполним нажмите Отмена \n'
                                    'и посоветуйтесь с коллегами по вашей проблеме.',
                                    '--ok-label=Далее', '--cancel-label=Отмена', '--width=600', '--height=400']).decode("utf-8").strip()

def step_2():
    if not os.path.exists(epson):
        if not os.path.exists(oki):
            global filename
            filename = subprocess.check_output(['zenity','--warning', '--title=Ошибка', '--text=Ошибка,работа программы остановлена,'
                                                                                        'не найден подключенный принтер в систему'])
            exit()
        else:
            print("Обнаружен принтер OKI")
    else:
        os.system(rmCom)
        os.system(lnLpt)
        from subprocess import Popen,PIPE
        proc = subprocess.Popen(fm1402, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
        filename = subprocess.check_output(['zenity', '--question', '--title=Установка СКНО ШАГ 2 Распечатываем отчеты из БЭП',
                                    '--text= "В фискальной утилите Снимите фискальный отчет за период расставив галочки в пункты: \n\n\n\n'
                                    '1.Данные о перерегистрациях\n'
                                    '2.Итоговые суммы из БЭП\n'
                                    '3.От Переррегистрации\n'
                                    '4.Печать в PC886\n'
                                    '5.Порт принтера LPT1(если у вас Epson) если OKI вероятно COM1\n'
                                    '(проверьте кнокой Проверка печати)\n\n\n'
                                    'После выполнения всех пунктов нажмите Далее.\nЕсли один из них не выполним нажмите Отмена \n'
                                    'и посоветуйтесь с коллегами по вашей проблеме', '--ok-label=Далее', '--cancel-label=Отмена', '--width=600', '--height=400']).decode("utf-8").strip()

def step_3():
    if not os.path.exists(DriveC + Name_BackUP):
        os.mkdir(DriveC + Name_BackUP)
    # если папка есть то проверяем ее на наличие ранее созданных бэкапов с именем Auto_BackUP
    else:
        list = os.listdir(TMP_STR)
        for i in list:
            if i == Name_BackUP:
                # Если такая папка уже есть переименовываем ее и выводим сообщение об этом
                os.rename('%s%s' % (TMP_STR, i), "%sAuto-BackUP-OLD-%s" % (TMP_STR, GetDate))
    # Создаем папку Auto-BackUP
    os.mkdir(TMP_STR + Dir_BackUP)
    # закрываем все программы
    os.system("sudo killall CashTerminal.exe")
    os.system("sudo killall RemoteModule.exe")
    os.system("sudo killall UnloadDiscountCard.exe")
    os.system("sudo killall dbsrv50.exe")
    os.system("sudo killall dbeng50.exe")
    os.system("sudo killall dbclient.exe")
    os.system("sudo killall OrdersClient.exe")
    # Копируем все нужные файлы
    copy_dir(PSTrade, BackUP, BackUP_PSTrade)
    copy_dir(FM1402, BackUP, BackUP_FM1402)
    copy_dir(DISCOUNT_Folder, BackUP, BackUP_DISCOUNT_Folder)
    copy_file(Cashmain_ini, BackUP, BackUP)
    copy_file(Cash_ini, BackUP, BackUP)
    copy_dir(Reports, BackUP, BackUP_Reports)
    # создаем папку для DLL и копируем их
    os.mkdir(BackUP_Sys32)
    copy_file(AdmFrame, BackUP, BackUP_Sys32)
    copy_file(borlndmm, BackUP, BackUP_Sys32)
    copy_file(Discount, BackUP, BackUP_Sys32)
    copy_file(Weights, BackUP, BackUP_Sys32)
    copy_file(PumpDLL, BackUP, BackUP_Sys32)
    copy_file(PsBackOffice, BackUP, BackUP_Sys32)

def step_4():
    global change
    change = subprocess.check_output(['zenity', '--list', '--radiolist', '--title=Установка СКНО ШАГ 3, Выбор кассы', '--text=Выберите какая касса(1-я(главная) или (2-я(не главная))',
                                                    '--column=Отметка выбора','--column=Вид кассы',
                                                    'True', '1', 'False', '2'])
    if change == b'1\n':
        remove_files()
        remove_dll()
        return change
    elif change == b'2\n':
        remove_files()
        remove_dll()
        return change

def step_5():
    ##############---Переменные----###########
    #Каталог с Install_Skno
    Dir_BackUP = os.getcwd()
    # папка с торговой программой PSTrade
    PSTrade = '/home/cashier/.wine/drive_c/PSTrade'
    # папка  файлов фискалки
    FM1402 = '/home/cashier/.wine/drive_c/FM1402'
    # Reports из System32
    Reports = '/home/cashier/.wine/drive_c/windows/system32/Reports'
    # Берем текущую дату
    GetDate = datetime.datetime.now().strftime("%d-%m-%Y--%H-%M-%S")
    # имя Бэкапа
    Name_BackUP = 'Auto-BackUP'
    # папка для BackUP's
    BackUP_PSTrade = Dir_BackUP + '/PSTrade'
    # папка для BackUP's
    BackUP_FM1402 = Dir_BackUP + '/FM1402'
    BackUP_Reports = Dir_BackUP + '/SYS/Reports'
    # BackUP-system32
    Sys32 = '/home/cashier/.wine/drive_c/windows/system32'
    # SynkAuto
    SynkAuto = Dir_BackUP + '/SYNC-auto.zip'
    # scripts
    scripts = '/home/cashier/scripts'
    #
    SynkAuto_file = '/home/cashier/scripts/SYNC-auto.zip'

    ########## DLL из System32
    AdmFrame = Dir_BackUP + '/SYS/AdmFrame.dll'
    borlndmm = Dir_BackUP + '/SYS/borlndmm.dll'
    Discount = Dir_BackUP + '/SYS/Discount.dll'
    Weights = Dir_BackUP + '/SYS/Weights.dll'
    PumpDLL = Dir_BackUP + '/SYS/PumpDLL.dll'
    PsBackOffice = Dir_BackUP + '/SYS/PsBackOffice.dll'
    AdmFrame_file = '/home/cashier/.wine/drive_c/windows/system32/AdmFrame.dll'
    borlndmm_file = '/home/cashier/.wine/drive_c/windows/system32/borlndmm.dll'
    Discount_file = '/home/cashier/.wine/drive_c/windows/system32/Discount.dll'
    Weights_file = '/home/cashier/.wine/drive_c/windows/system32/Weights.dll'
    PumpDLL_file = '/home/cashier/.wine/drive_c/windows/system32/PumpDLL.dll'
    PsBackOffice_file = '/home/cashier/.wine/drive_c/windows/system32/PsBackOffice.dll'
    usb99_file = '/etc/udev/rules.d/99-usb-serial.rules'
    MCTA_vendor = 'SUBSYSTEM=="tty", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="5740", SYMLINK+="mcta"'

    def pstrade_copy(SRC_PATH, DST_PATH):
        for f in os.listdir(SRC_PATH):
            if os.path.isfile(os.path.join(SRC_PATH, f)):
                shutil.copy(os.path.join(SRC_PATH, f), os.path.join(DST_PATH, f))
            if os.path.isdir(os.path.join(SRC_PATH, f)):
                if f == "Adm":
                    tmp = SRC_PATH + '/' + f + '/RemoteModule.exe'
                    adm = '/home/cashier/.wine/drive_c/PSTrade/Adm'
                    shutil.copy(tmp, adm)
                elif f == "OrdersClient":
                    tmp = SRC_PATH + '/' + f
                    OrdClient = '/home/cashier/.wine/drive_c/PSTrade/OrdersClient'
                    for i in os.listdir(tmp):
                        if os.path.isfile(os.path.join(tmp, i)):
                            shutil.copy(os.path.join(tmp, i), os.path.join(OrdClient, i))
                else:
                    shutil.copytree(os.path.join(SRC_PATH, f), os.path.join(DST_PATH, f))

    # функция копирования каталогов
    def copypath(SRC_PATH, DST_PATH):
        # проверка существования каталога в папке с бэкапом
        if not os.path.exists(SRC_PATH):
            warning_error_copy_sknoFiles(SRC_PATH)
        else:
            # проверка существования каталога в образе
            if not os.path.exists(DST_PATH):
                # если нет то копируем
                try:
                    shutil.copytree(SRC_PATH, DST_PATH)
                except Exception:
                    warning_process_copy(SRC_PATH, DST_PATH)
            else:
                # если есть переименовываем старую,кладем новую
                os.rename(DST_PATH, DST_PATH + '-OLD-' + GetDate)
                try:
                    shutil.copy(SRC_PATH, DST_PATH)
                except Exception:
                    warning_process_copy(SRC_PATH, DST_PATH)

    # функция копирования файлов
    def copydll(SRC_PATH, DST_PATH, NAME_FILE):
        # проверка существования каталога в папке с бэкапом
        if not os.path.exists(SRC_PATH):
            # если не найдена выдаем соббщение
            warning_error_copy_sknoFiles(SRC_PATH)
        else:
            # проверка существования файла в образе
            if not os.path.exists(NAME_FILE):
                # если нет то копируем
                try:
                    shutil.copy(SRC_PATH, DST_PATH)
                except Exception:
                    warning_process_copy(SRC_PATH, DST_PATH)
            else:
                # если есть переименовываем старый,кладем новый
                os.remove(NAME_FILE)
                try:
                    shutil.copy(SRC_PATH, DST_PATH)
                except Exception:
                    warning_process_copy(SRC_PATH, DST_PATH)

    # копируем файлы
    def copy_1Kass():
        copypath(SRC_PATH=BackUP_FM1402, DST_PATH=FM1402)
        pstrade_copy(BackUP_PSTrade, PSTrade)
        copypath(SRC_PATH=BackUP_Reports, DST_PATH=Reports)
        copydll(SRC_PATH=SynkAuto, DST_PATH=scripts, NAME_FILE=SynkAuto_file)
        copydll(SRC_PATH=AdmFrame, DST_PATH=Sys32, NAME_FILE=AdmFrame_file)
        copydll(SRC_PATH=Discount, DST_PATH=Sys32, NAME_FILE=Discount_file)
        copydll(SRC_PATH=Weights, DST_PATH=Sys32, NAME_FILE=Weights_file)
        copydll(SRC_PATH=PumpDLL, DST_PATH=Sys32, NAME_FILE=PumpDLL_file)
        copydll(SRC_PATH=borlndmm, DST_PATH=Sys32, NAME_FILE=borlndmm_file)
        copydll(SRC_PATH=PsBackOffice, DST_PATH=Sys32, NAME_FILE=PsBackOffice_file)

    def copy_2Kass():
        copypath(SRC_PATH=BackUP_FM1402, DST_PATH=FM1402)
        pstrade_copy(BackUP_PSTrade, PSTrade)
        copypath(SRC_PATH=BackUP_Reports, DST_PATH=Reports)
        copydll(SRC_PATH=SynkAuto, DST_PATH=scripts, NAME_FILE=SynkAuto_file)
        copydll(SRC_PATH=AdmFrame, DST_PATH=Sys32, NAME_FILE=AdmFrame_file)
        copydll(SRC_PATH=Discount, DST_PATH=Sys32, NAME_FILE=Discount_file)
        copydll(SRC_PATH=Weights, DST_PATH=Sys32, NAME_FILE=Weights_file)
        copydll(SRC_PATH=PumpDLL, DST_PATH=Sys32, NAME_FILE=PumpDLL_file)
        copydll(SRC_PATH=borlndmm, DST_PATH=Sys32, NAME_FILE=borlndmm_file)
        copydll(SRC_PATH=PsBackOffice, DST_PATH=Sys32, NAME_FILE=PsBackOffice_file)
        from subprocess import Popen, PIPE
        proc = subprocess.Popen(rmDB, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)

    if change == b'1\n':
        copy_1Kass()
    elif change == b'2\n':
        copy_2Kass()

    SynkAuto_zip = zipfile.ZipFile(SynkAuto_file)
    SynkAuto_zip.extractall(scripts)
    SynkAuto_zip.close()
    os.system('bash /home/cashier/scripts/SYNC-auto/install.sh')

    add_string_99usb()

def step_6():
    from subprocess import Popen, PIPE
    proc = subprocess.Popen(fm1402, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
    filename = subprocess.check_output(['zenity', '--question', '--title=Установка СКНО ШАГ 4 Регистрируем БЭП',
         '--text= "В фискальной утилите произведите регистрацию КСА\n'
         'Распечатайте отчет расставив галочки в пункты:  \n\n\n\n'
         '1.Данные о перерегистрациях\n'
         '2.Данные за период\n'
         '3.Печать в PC886\n'
         '4.Порт принтера LPT1(если у вас Epson) если OKI вероятно COM1\n'
         '(проверьте кнокой Проверка печати)\n\n\n'
         'После выполнения всех пунктов нажмите Далее.\nЕсли один из них не выполним нажмите Отмена \n'
         'и посоветуйтесь с коллегами по вашей проблеме', '--ok-label=Далее', '--cancel-label=Отмена', '--width=600',
         '--height=400']).decode("utf-8").strip()

def step_7():

    addCashmain = os.getcwd() + '/AddCashMain.ini'

    text = ''
    bool = 0

    def ReplaceLineInFile(fileName, sourceText, replaceText):
        global text
        global bool

        if bool == 0:
            file = open(fileName, encoding='cp866', mode='r')
            text = file.read()
            file.close()
            text = text.replace(sourceText, replaceText)
            bool = 1
        else:
            text = text.replace(sourceText, replaceText)



    f = open(addCashmain, 'r', encoding='cp866')
    for oneline in f:
        if oneline == '\n':
            continue
        else:
            oneline = oneline.strip('')
            tmp_string = str(oneline)
            ReplaceLineInFile(Cashmain_ini, tmp_string, newString)
    f.close()

    s = list(text)
    i = 2
    while i < len(s):
        if s[i - 2] == '\n' and s[i - 1] == '\n' and s[i] == '\n':
            s.pop(i - 1)
        i += 1
    i = 0
    while s[i] == '\n':
        s.pop(i)

    text = ''.join(s)

    file = open(Cashmain_ini, encoding='cp866', mode='w')
    file.write(text)
    file.close()


def step_8():
    global change
    if change == b'1\n':
        scview_pass()
        from subprocess import Popen, PIPE
        proc = subprocess.Popen('scview', shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
        filename = subprocess.check_output(['zenity', '--question', '--title=Установка СКНО ШАГ 5 Добавляем пользователя в БД',
                                            '--text= "Добавьте пользователя в БД\n'
                                            '\n\n\n\n,'
                                            '%s \n'
                                            '%s \n\n'
                                            'После выполнения всех пунктов нажмите Далее.\nЕсли один из них не выполним нажмите Отмена \n'
                                            'и посоветуйтесь с коллегами по вашей проблеме', '--ok-label=Далее',
                                            '--cancel-label=Отмена', '--width=600',
                                            '--height=400'] % (name, passw)).decode("utf-8").strip()
        return change
    elif change == b'2\n':
        return change



step_1()
if [ "$?" == 0 ]:
    step_2()
    if [ "$?" == 0 ]:
        step_3()
        step_4()
        step_5()
        step_6()
        step_7()



